#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

#include "mem_internals.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args); // NOLINT 
  va_end (args);
  abort();
}

extern inline size_t size_max( size_t x, size_t y );

void blocks_stack_push(struct blocks_stack** stack, struct block_header* block) {
  struct blocks_stack* node = malloc(sizeof(struct blocks_stack));
  node->block = block;
  node->next = *stack;
  *stack = node;
}

struct block_header* blocks_stack_pop(struct blocks_stack** stack) {
  struct blocks_stack* node = *stack;
  if (!node) return NULL;
  struct block_header* block = node->block;
  struct blocks_stack* next_node = node->next;
  free(node);
  *stack = next_node;
  return block;
}

void blocks_stack_remove(struct blocks_stack** stack, struct block_header* block) {
  struct blocks_stack* prev_node = NULL;
  struct blocks_stack* node = *stack;
  while (node) {
    if (node->block == block) {
      if (prev_node) {
        prev_node->next = node->next;
        free(node);
      }
      else blocks_stack_pop(stack);
      break;
    }
    prev_node = node;
    node = node->next;
  }
}

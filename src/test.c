#include "test.h"

#define BLOCKS_COUNT_FOR_TEST_MALLOC_1_BLOCK 1
#define BLOCK0_SIZE_FOR_TEST_MALLOC_1_BLOCK 4242
bool test_malloc_1_block(struct blocks_stack** blocks, FILE* file) {
    void* block0 = NULL;
    malloc_blocks(blocks, file, BLOCKS_COUNT_FOR_TEST_MALLOC_1_BLOCK, &block0, BLOCK0_SIZE_FOR_TEST_MALLOC_1_BLOCK);

    bool result = false;

    struct block_header* block0_header = block_get_header(block0);
    if (block0_header) result = (block0_header->capacity.bytes == BLOCK0_SIZE_FOR_TEST_MALLOC_1_BLOCK) && 
                                (block0_header->is_free == false);

    return result;
}


#define BLOCKS_COUNT_FOR_TEST_MALLOC_3_BLOCKS_FREE_1 3
#define BLOCK0_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_1 1000
#define BLOCK1_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_1 2000
#define BLOCK2_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_1 3000
bool test_malloc_3_blocks_free_1(struct blocks_stack** blocks, FILE* file) {
    void* block0 = NULL;
    void* block1 = NULL;
    void* block2 = NULL;
    malloc_blocks(blocks, file, BLOCKS_COUNT_FOR_TEST_MALLOC_3_BLOCKS_FREE_1, &block0, BLOCK0_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_1, &block1, BLOCK1_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_1, &block2, BLOCK2_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_1);

    free_block(block1, blocks, file);

    bool result = false;
    
    struct block_header* block0_header = block_get_header(block0);
    struct block_header* block1_header = block_get_header(block1);
    struct block_header* block2_header = block_get_header(block2);

    if (block0_header && block1_header && block2_header) result = (block0_header->capacity.bytes == BLOCK0_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_1) && 
                                                                  (block0_header->is_free == false) &&
                                                                  (block1_header->capacity.bytes == BLOCK1_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_1) && 
                                                                  (block1_header->is_free == true) &&
                                                                  (block2_header->capacity.bytes == BLOCK2_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_1) &&
                                                                  (block2_header->is_free == false);

    return result;
}


#define BLOCKS_COUNT_FOR_TEST_MALLOC_3_BLOCKS_FREE_2 3
#define BLOCK0_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_2 1000
#define BLOCK1_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_2 2000
#define BLOCK2_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_2 3000
bool test_malloc_3_blocks_free_2(struct blocks_stack** blocks, FILE* file) {
    void* block0 = NULL;
    void* block1 = NULL;
    void* block2 = NULL;
    malloc_blocks(blocks, file, BLOCKS_COUNT_FOR_TEST_MALLOC_3_BLOCKS_FREE_2, &block0, BLOCK0_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_2, &block1, BLOCK1_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_2, &block2, BLOCK2_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_2);

    free_block(block1, blocks, file);
    free_block(block0, blocks, file);

    bool result = false;
    
    struct block_header* block0_header = block_get_header(block0);
    struct block_header* block1_header = block_get_header(block2);

    if (block0_header && block1_header) result = (block0_header->capacity.bytes == (BLOCK2_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_2 + offsetof(struct block_header, contents))) && 
                                                 (block0_header->is_free == true) &&
                                                 (block1_header->capacity.bytes == BLOCK2_SIZE_FOR_TEST_MALLOC_3_BLOCKS_FREE_2) &&
                                                 (block1_header->is_free == false);

    return result;
}


#define BLOCKS_COUNT_FOR_TEST_NEW_REGION_EXTENDS 2
#define BLOCK0_SIZE_FOR_TEST_NEW_REGION_EXTENDS 1000
#define BLOCK1_SIZE_FOR_TEST_NEW_REGION_EXTENDS 7000
bool test_new_region_extends(struct blocks_stack** blocks, FILE* file) {
    struct block_header* heap = HEAP_START;
    size_t block0_size = heap->capacity.bytes - BLOCK0_SIZE_FOR_TEST_NEW_REGION_EXTENDS;

    void* block0 = NULL;
    void* block1 = NULL;
    malloc_blocks(blocks, file, BLOCKS_COUNT_FOR_TEST_NEW_REGION_EXTENDS, &block0, block0_size, &block1, BLOCK1_SIZE_FOR_TEST_NEW_REGION_EXTENDS);


    bool result = false;
    
    struct block_header* block0_header = block_get_header(block0);
    struct block_header* block1_header = block_get_header(block1);

    if (block0_header && block1_header) result = (block0_header->capacity.bytes == block0_size) && 
                                                 (block0_header->is_free == false) &&
                                                 (block1_header->capacity.bytes == BLOCK1_SIZE_FOR_TEST_NEW_REGION_EXTENDS) &&
                                                 (block1_header->is_free == false);

    return result;
}


#define BLOCK0_SIZE_FOR_TEST_NEW_REGION_NOT_EXTENDS 1000
#define BLOCK1_SIZE_FOR_TEST_NEW_REGION_NOT_EXTENDS 7000
bool test_new_region_not_extends(struct blocks_stack** blocks, FILE* file) {
    struct block_header* heap = HEAP_START;
    size_t block0_size = heap->capacity.bytes - BLOCK0_SIZE_FOR_TEST_NEW_REGION_NOT_EXTENDS;

    void* block0 = NULL;
    malloc_blocks(blocks, file, 1, &block0, block0_size);

    // synthetically map extending region
    alloc_region(block0 + block0_size + BLOCK0_SIZE_FOR_TEST_NEW_REGION_NOT_EXTENDS, REGION_MIN_SIZE);

    void* block1 = NULL;
    malloc_blocks(blocks, file, 1, &block1, BLOCK1_SIZE_FOR_TEST_NEW_REGION_NOT_EXTENDS);

    bool result = false;
    
    struct block_header* block0_header = block_get_header(block0);
    struct block_header* block1_header = block_get_header(block0 + block0_size + offsetof(struct block_header, contents));
    struct block_header* block2_header = block_get_header(block1);

    if (block0_header && block1_header && block2_header) result = (block0_header->capacity.bytes == block0_size) && 
                                                                  (block0_header->is_free == false) &&
                                                                  (block1_header->capacity.bytes == (BLOCK0_SIZE_FOR_TEST_NEW_REGION_NOT_EXTENDS - offsetof(struct block_header, contents))) &&
                                                                  (block1_header->is_free == true) &&
                                                                  (block2_header->capacity.bytes == BLOCK1_SIZE_FOR_TEST_NEW_REGION_NOT_EXTENDS) &&
                                                                  (block2_header->is_free == false);

    return result;

}

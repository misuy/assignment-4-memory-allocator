#ifndef _TEST_H_
#define _TEST_H_

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "mem.h"
#include "mem_internals.h"
#include "test_util.h"
#include "util.h"

bool test_malloc_1_block(struct blocks_stack** blocks, FILE* file);
bool test_malloc_3_blocks_free_1(struct blocks_stack** blocks, FILE* file);
bool test_malloc_3_blocks_free_2(struct blocks_stack** blocks, FILE* file);
bool test_new_region_extends(struct blocks_stack** blocks, FILE* file);
bool test_new_region_not_extends(struct blocks_stack** blocks, FILE* file);

#endif

#ifndef _UTIL_H_
#define _UTIL_H_

#include <stddef.h>

struct blocks_stack {
  struct block_header* block;
  struct blocks_stack* next;
};

inline size_t size_max( size_t x, size_t y ) { return (x >= y)? x : y ; }

_Noreturn void err( const char* msg, ... );

void blocks_stack_push(struct blocks_stack** stack, struct block_header* block);
struct block_header* blocks_stack_pop(struct blocks_stack** stack);
void blocks_stack_remove(struct blocks_stack** stack, struct block_header* block);

#endif

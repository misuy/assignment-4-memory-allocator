#ifndef _MEM_H_
#define _MEM_H_


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <sys/mman.h>

#include "mem_internals.h"

#define HEAP_START ((void*)0x04040000)

struct region alloc_region  ( void const * addr, size_t query );
bool try_merge_with_next( struct block_header* block );

void* _malloc( size_t query );
void  _free( void* mem );
void* heap_init( size_t initial_size );

#define DEBUG_FIRST_BYTES 4

void debug_struct_info( FILE* f, void const* address );
void debug_heap( FILE* f,  void const* ptr );

struct block_header* block_get_header(void* contents);

#endif

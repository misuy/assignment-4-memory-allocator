#ifndef _TEST_UTIL_H_
#define _TEST_UTIL_H_

#include <stdbool.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"
#include "test.h"
#include "util.h"

void* malloc_block(size_t size, struct blocks_stack** blocks, FILE* file);
void malloc_blocks(struct blocks_stack** blocks, FILE* file, size_t blocks_count, ...);
void free_block(void* block, struct blocks_stack** blocks, FILE* file);

void perform_all_tests(FILE* file);

#endif

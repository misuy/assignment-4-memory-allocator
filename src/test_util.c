#include "test_util.h"

#include <stdarg.h>
#include <stdlib.h>

static void print_block(struct block_header* block, FILE* file) {
    fprintf(file, "block( addr=%zu, capacity=%zu, is_free=%d )", (size_t) block, block->capacity.bytes, block->is_free);
}

static void print_blocks(void* start_block, FILE* file) {
    fprintf(file, "Heap: ");
    if (!start_block) fprintf(file, "NULL\n");
    else {
        struct block_header* block = (struct block_header*) start_block;
        
        while (block) {
            print_block(block, file);
            fprintf(file, " -> ");
            block = block->next;
        }

        fprintf(file, "NULL\n");
    }
}

static void print_test_run(void* heap, char* test_name, FILE* file) {
    fprintf(file, "Running test(%s)...\n", test_name);
    fprintf(file, "Heap before: ");
    print_blocks(heap, file);
}

static void print_test_finished(void* heap, char* test_name, bool result, FILE* file) {
    fprintf(file, "Heap after: ");
    print_blocks(heap, file);
    fprintf(file, "Test(%s) finished. Result=%d.\n", test_name, result);
}

void* malloc_block(size_t size, struct blocks_stack** blocks, FILE* file) {
    void *block = _malloc(size);
    struct block_header* header = block_get_header(block);

    fprintf(file, "Creating ");
    print_block(header, file);
    fprintf(file, "\n");

    blocks_stack_push(blocks, header);
    return block;
}

void malloc_blocks(struct blocks_stack** blocks, FILE* file, size_t blocks_count, ...) {
    // uncomment this (clang-tidy bug)
    /*
    va_list factor;
    va_start(factor, blocks_count);
    for (size_t i=0; i<blocks_count; i++) {
        void** pointer = va_arg(factor, void**);
        *pointer = malloc_block(va_arg(factor, size_t), blocks, file);
    }
    va_end(factor);
    */
}

static void free_block_without_removing_from_stack(struct block_header* header, FILE* file) {
    fprintf(file, "Freeing ");

    print_block(header, file);
    _free(header->contents);

    fprintf(file, "\n");
}

void free_block(void* block, struct blocks_stack** blocks, FILE* file) {
    struct block_header* header = block_get_header(block);
    blocks_stack_remove(blocks, header);
    free_block_without_removing_from_stack(header, file);
}

static void restore_heap(struct blocks_stack** blocks, FILE* file) {
    struct block_header* block = NULL;
    fprintf(file, "Restoring heap...\n");
    while ((block = blocks_stack_pop(blocks))) {
        free_block_without_removing_from_stack(block, file);
    }

    fprintf(file, "Heap restored.\n");
}



bool perform_test(bool test (struct blocks_stack** blocks, FILE* file), void* heap, char* test_name, FILE* file) {
    fprintf(file, "------------------------------------------------\n");

    print_test_run(heap, test_name, file);

    struct blocks_stack* blocks = NULL;

    bool result = test(&blocks, file);

    print_test_finished(heap, test_name, result, file);
    fprintf(file, "\n");

    restore_heap(&blocks, file);
    try_merge_with_next(heap);

    fprintf(file, "------------------------------------------------\n\n");

    return result;
}



void perform_all_tests(FILE* file) {
    fprintf(file, "Running tests...\n\n");

    void* heap = heap_init(0);
    
    uint64_t tests_count = 0;
    uint64_t passed_tests_count = 0;

    if (perform_test(&test_malloc_1_block, heap, "malloc 1 block", file)) passed_tests_count += 1;
    tests_count += 1;

    if (perform_test(&test_malloc_3_blocks_free_1, heap, "malloc 3 blocks free second", file)) passed_tests_count += 1;
    tests_count += 1;

    if (perform_test(&test_malloc_3_blocks_free_2, heap, "malloc 3 blocks free first and second", file)) passed_tests_count += 1;
    tests_count += 1;

    if (perform_test(&test_new_region_extends, heap, "new region extends", file)) passed_tests_count += 1;
    tests_count += 1;

    if (perform_test(&test_new_region_not_extends, heap, "new region not extends", file)) passed_tests_count += 1;
    tests_count += 1;

    fprintf(file, "%" PRIu64 " tests out of %" PRIu64 " passed\n", passed_tests_count, tests_count);
}